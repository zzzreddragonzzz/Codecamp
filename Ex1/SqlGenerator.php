<?php
  class SqlGenerator
  {

    public static function select($tableName, $columns) {
      return "select " . join(',',$columns). " from {$tableName}";
    }

    public static function insert($tableName, $columns, $values) {
      $insertValues = self::getInsertValuesQuery($values);
      return "insert into (" . join(',',$columns) . ") values ({$insertValues})";
    }

    public static function delete($tableName, $conditon) {
      return "delete form {$tableName} where {$conditon}";
    }

    public static function update($tableName, $columns, $values, $conditon) {
      $updateColumns = self::getUpdateColumnsQuery($columns, $values);
      return "update {$tableName} set {$updateColumns} where {$conditon}";
    }

    private function getInsertValuesQuery($values) {
      $insertValues = "";
      for($i=0; $i<count($values); $i++) {
        $insertValues .= "'".$values[$i]."'";
        if ( $i < count($values) - 1 ) {
          $insertValues .= "'".$values[$i]."', ";
        }
      }
      return $insertValues;
    }

    private function getUpdateColumnsQuery($columns, $values) {
      $updateColumns = "";

      for($i=0; $i<count($columns); $i++) {
        $updateColumns .= $columns[$i] . " = '" . $values[$i] . "'";
        if ( $i < count($columns) - 1 ) {
          $updateColumns .= $columns[$i] . " = '" . $values[$i] . "', ";
        }
      }
      return $updateColumns;
    }
  }

 var_dump(SqlGenerator::select("hocsinh",["ten","lop"]));
 var_dump(SqlGenerator::insert("hocsinh",["ten","lop"], ["minh","1A"]));
 var_dump(SqlGenerator::delete("hocsinh", "id='1'"));
 var_dump(SqlGenerator::update("hocsinh",["ten","lop"], ["minh","1A"], "id='1'"));
?>
