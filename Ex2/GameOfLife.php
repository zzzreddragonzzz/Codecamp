<?php
/**
*
*/
class GameOfLife
{
  private $cells;
  private $rows;
  private $columns;

  public function __construct($cells, $rows, $columns) {
    // print_r($cells);
    $this->rows = $rows;
    $this->columns = $columns;
    for($i = 0; $i < count($rows); $i++) {
      for($j = 0; $j < count($columns); $j++) {
        // echo $cells[$i][$j];
        $this->cells[$i][$j] = new Cell($j, $i, $cells[$i][$j]);
        // print_r($cellsRow);
      }
    }
  }

  public function getCells() {
    return $this->cells;
  }

  public function nextGeneration( ) {
    for($i = 0; $i < count($this->rows); $i++) {
      for($j = 0; $j < count($this->columns); $j++) {
        $this->cells[$i][$j]->setStatus($this->cells[$i][$j]->caculateAliveOrDead());
      }
    }
  }
}

class Cell
{
  private $x;
  private $y;
  private $status;

  public function __construct($x, $y, $status) {
    $this->x = $x;
    $this->y = $y;
    $this->status = $status;
  }

  public function getX() {
    return $this->x;
  }

  public function getY() {
    return $this->y;
  }

  public function getStatus() {
    return $this->status;
  }

  public function setStatus($status) {
    $this->status = $status;
  }

  public function caculateAliveOrDead() {
    $neighbors =
        CountAlive(new Cell($this->getX() - 1, $this->getY() - 1)) +
        CountAlive(new Cell($this->getX() - 1, $this->getY())) +
        CountAlive(new Cell($this->getX() - 1, $this->getY() + 1)) +
        CountAlive(new Cell($this->getX() + 1, $this->getY() + 1)) +
        CountAlive(new Cell($this->getX() + 1, $this->getY())) +
        CountAlive(new Cell($this->getX() + 1, $this->getY() - 1)) +
        CountAlive(new Cell($this->getX(), $this->getY() -1)) +
        CountAlive(new Cell($this->getX(), $this->getY() + 1));


    if ($this->getStatus() && ($neighbors < 2 || $neighbors > 3)) {
      return 0;
    }

    if (!$this->getStatus() && $neighbors == 3) {
      return 1;
    }

    return $this->getStatus();
  }

  private function CountAlive(Cell $cell) {
    return $cell->getStatus() ?  1 : 0;
  }
}

$cellsInput = array(
  array(0, 0, 0, 0, 0, 0, 0, 0),
  array(0, 0, 0, 0, 1, 0, 0, 0),
  array(0, 0, 0, 1, 1, 0, 0, 0),
  array(0, 0, 0, 0, 0, 0, 0, 0)
);

// print_r($cellsInput[2]);
// echo $cellsInput[2][3];
$GameOfLife = new GameOfLife($cellsInput, 4, 8);
$GameOfLife->nextGeneration();
var_dump($GameOfLife->getCells());

?>
