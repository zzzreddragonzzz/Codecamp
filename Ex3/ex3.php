<?php

$dictionary = [
    "ABC" => 1,
    "BCD" => 5,
    "CDE" => 25,
    "DEF" => 77,
    "EFG" => 4,
    "FGH" => 24,
    "GHI" => 4,
    "HIJ" => 144,
    "IJK" => 384,
    "JKL" => 276,
    "KLM" => 13,
    "LMN" => 2,
    "MNO" => 10036466,
    "NOP" => 279,
    "OPQ" => 9731
];

$random = rand(0, 252028800);
$terms = [12, 24, 36, 48, 60];
$mileages = [6000, 10000, 15000, 20000, 25000, 30000, 40000, 50000];

$file = fopen('example-input.txt', 'w+');
foreach ($dictionary as $key => $pair) {
    for ($i = 0; $i < $pair; $i++) {
        $elementId = rand(1, 10);
        $next = rand(100000, 999999);
        $term = $terms[rand(0, count($terms) - 1)];
        $mileage = $mileages[rand(0, count($mileages) - 1)];
        $line = "{$key},{$elementId},{$next},{$term},{$mileage}," . rand(1, 100) . "." . rand(1, 99) . ",,\n";
        $fw = fwrite($file, $line);

    }
}

fclose($file);
