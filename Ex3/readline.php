<?php
$start = microtime(true);

$filename = 'example-input.txt';

$file = new SplFileObject($filename, "r");
$contents = $file->fread($file->getSize());

// $fileHandle = fopen('example-input.txt', 'r');

$lineCount = 0;

if ($contents) {
    while(($line = fgets($contents)) != false) {
        $lineCount++;
    }
} else {
    die("read file err");
}

$time_elapsed_secs = microtime(true) - $start;
$output = "Total lines: " . $lineCount . "time: " . $time_elapsed_secs;

echo $output;



